#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animal sound " << std::endl;
    }
};

class Dog : public Animal
{
public:
        void Voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!" << std::endl;
    }
};

class Bird : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Tweet!" << std::endl;
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Mooo!" << std::endl;
    }
};

class Wolf : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Owoooo!" << std::endl;
    }
};

int main()
{
    Animal* animals[] = { new Dog(), new Cat(), new Bird(), new Cow(), new Wolf() };

    for (int i = 0; i < 5; i++)
    {
        animals[i]->Voice();
        delete animals[i];
    }
    return 0;
}
